package mikerp99.network.simulation.proc.l2;


import mikerp99.network.simulation.data.PDU;
import mikerp99.network.simulation.data.l2.ethernet.MacAddress;
import mikerp99.network.simulation.proc.PDUHandler;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SwitchFibTest {


    @Test
    public void getHandler() {
        SwitchFib switchFib = new SwitchFib();
        MacAddress macAddress1 = new MacAddress("aaa1.bbbb.cccc");
        TestPDUHandler testPDUHandler1 = new TestPDUHandler();

        MacAddress macAddress2 = new MacAddress("aaa2.bbbb.cccc");
        TestPDUHandler testPDUHandler2 = new TestPDUHandler();

        switchFib.insert(macAddress1, testPDUHandler1);
        assertEquals(testPDUHandler1, switchFib.getHandler(macAddress1).get());

        switchFib.insert(macAddress2, testPDUHandler2);
        assertEquals(testPDUHandler2, switchFib.getHandler(macAddress2).get());
    }

    @Test
    public void noHandlerForMacWhenNotPresent(){
        SwitchFib switchFib = new SwitchFib();
        MacAddress macAddress3 = new MacAddress("aaa3.bbbb.cccc");
        assertEquals(false, switchFib.getHandler(macAddress3).isPresent());
    }

    @Test
    public void multipleMacAddressesForOneHandler(){
        SwitchFib switchFib = new SwitchFib();
        MacAddress macAddress1 = new MacAddress("aaa1.bbbb.cccc");
        MacAddress macAddress2 = new MacAddress("aaa2.bbbb.cccc");
        TestPDUHandler testPDUHandler1 = new TestPDUHandler();

        switchFib.insert(macAddress1, testPDUHandler1);
        switchFib.insert(macAddress2, testPDUHandler1);
        assertEquals(testPDUHandler1, switchFib.getHandler(macAddress1).get());
        assertEquals(testPDUHandler1, switchFib.getHandler(macAddress2).get());
    }

    @Test
    public void relearnNewHandler(){
        SwitchFib switchFib = new SwitchFib();
        MacAddress macAddress1 = new MacAddress("aaa1.bbbb.cccc");
        MacAddress macAddress2 = new MacAddress("aaa2.bbbb.cccc");
        TestPDUHandler testPDUHandler1 = new TestPDUHandler();
        TestPDUHandler testPDUHandler2 = new TestPDUHandler();

        switchFib.insert(macAddress1, testPDUHandler1);
        switchFib.insert(macAddress2, testPDUHandler1);
        assertEquals(testPDUHandler1, switchFib.getHandler(macAddress1).get());
        assertEquals(testPDUHandler1, switchFib.getHandler(macAddress2).get());

        switchFib.insert(macAddress2, testPDUHandler2);
        assertEquals(testPDUHandler1, switchFib.getHandler(macAddress1).get());
        assertEquals(testPDUHandler2, switchFib.getHandler(macAddress2).get());
    }

    @Test
    public void ageOut(){
        SwitchFib switchFib = new SwitchFib(1);
        MacAddress macAddress1 = new MacAddress("aaa1.bbbb.cccc");
        TestPDUHandler testPDUHandler1 = new TestPDUHandler();
        switchFib.insert(macAddress1, testPDUHandler1);
        assertEquals(testPDUHandler1, switchFib.getHandler(macAddress1).get());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertFalse(switchFib.getHandler(macAddress1).isPresent());

        switchFib.insert(macAddress1, testPDUHandler1);
        assertEquals(testPDUHandler1, switchFib.getHandler(macAddress1).get());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertFalse(switchFib.getHandler(macAddress1).isPresent());
    }

    class TestPDUHandler implements PDUHandler{

        @Override
        public void receive(PDU pdu) {

        }

        @Override
        public void send(PDU pdu) {

        }
    }

}
