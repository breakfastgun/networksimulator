package mikerp99.network.simulation.data;

public abstract class PDUInfo {

    PDUType encapsulatedDataType;

    public PDUInfo(PDUType encapsulatedDataType) {
        this.encapsulatedDataType = encapsulatedDataType;
    }

    public PDUType getEncapsulatedDataType() {
        return encapsulatedDataType;
    }

}
