package mikerp99.network.simulation.data.l3.ip;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IPv4SubnetMath {

	private long network;
	private long subnetmask;
	private long wildcardmask;
	private long broadcast;
	public final long all1s = Long.valueOf("4294967295");
	
	public IPv4SubnetMath(){
		
	}
	
	@Override
	public int hashCode(){
		return (int) (network + broadcast);
	}
	
	public String networkPlusN(int n){
		long net = this.network;
		net = net + n;
		String ip = decimalToDottedDecimal(net);
		return ip;
	}
	
	public void setNetwork(long network){
		this.network = network;
	}
	
	public void setNetwork(String network) {
		long n = 0;
		
		n = dottedDecimalToDecimal(network);
		
		
		
		this.network = n;
	}
	
	public long ipsInSubnet(){
		return this.broadcast - (this.network - 1);
	}
	
	public long getNetwork() {
		return network;
	}

	public String getNetworkDottedDecimal() {
		String net = "";
		net = decimalToDottedDecimal(this.network);
		return net;
	}
	
	public String getSubnetMaskDottedDecimal(){
		String snm = "";
		snm = decimalToDottedDecimal(this.subnetmask);
		return snm;
	}
	
	public int getSubnetBitLength(){
		int snm = 0;
		String bin = Long.toBinaryString(this.subnetmask);
		for (String s: bin.split("")){
			if (s.equalsIgnoreCase("1"))
				snm++;
		}
		return snm;
	}
	
	public String getBroadcastDottedDecimal() {
		String bc = "";
		bc = decimalToDottedDecimal(this.broadcast);
		return bc;
	}
	
	public void setSubnetmask(String subnetmask) {
		long n = 0;
		if (subnetmask.matches("[\\/\\\\]?(\\d+)")){
			subnetmask = subnetmask.replace("\\", "");
			subnetmask = subnetmask.replace("/", "");
			int maskBitLength = Integer.valueOf(subnetmask);
			String snmbin = "";
			for (int i = 1; i <= maskBitLength; i++){
				snmbin = snmbin + "1";
			}
			for (int i = 1; i <= 32 - maskBitLength ; i++){
				snmbin = snmbin + "0";
			}
			
			n = Long.parseLong(snmbin,2);
			this.wildcardmask = all1s ^ n;
			this.network = this.network & n;
			//System.out.print("all1s: " + all1s + " ^ network: " + this.network + "\n");
			//System.out.print("network: " + this.network + " ^ wcm: " + this.wildcardmask + "\n");
			this.broadcast = this.network ^ this.wildcardmask;
			
			this.subnetmask = n;
			return;
		}else if (subnetmask.matches("")){
			
		}
		n = dottedDecimalToDecimal(subnetmask);
		//long all1s = 256 * 256 * 256 * 256 - 1;
		this.wildcardmask = all1s ^ n;
		this.network = this.network & n;
		this.broadcast = this.network ^ this.wildcardmask;
		
		this.subnetmask = n;
	}
	
//	public void setSubnetmask(Integer maskBitLength) {
//		long n = 0;
//		System.out.print("hello\n");
//		String snmbin = "";
//		for (int i = 1; i <= maskBitLength; i++){
//			snmbin = snmbin + "1";
//		}
//		for (int i = 1; i <= maskBitLength - 32; i++){
//			snmbin = snmbin + "0";
//		}
//		System.out.print("SNM BIN: " + snmbin + "\n");
//		n = Long.parseLong(snmbin,2);
//		
//		this.wildcardmask = all1s ^ n;
//		this.network = this.network & n;
//		this.broadcast = this.network ^ this.wildcardmask;
//		
//		this.subnetmask = n;// decimalToDottedDecimal(Long.toString(n));
//	}

	public void setSubnetmask(long subnetmask) {
		this.subnetmask = subnetmask;
	}

	public long getSubnetmask() {
		return subnetmask;
	}
	
	public long getWildcardmask() {
		return wildcardmask;
	}

	public void setWildcardmask(long wildcardmask) {
		this.wildcardmask = wildcardmask;
	}

	public long broadcast() {
		return broadcast;
	}
	
	public String getBroadcast(){
		String bc = "";
		
		
		return bc;
	}
	
	public static String decimalToDottedDecimal(long dec){
		String dd = "";
		String bin = Long.toBinaryString(dec);
		
		if (bin.length() < 32){
			int morezeros = 32 - bin.length();
			for (int i = 1; i <= morezeros; i++){
				bin = "0"+bin;
			}
		}
		
		Pattern p = Pattern.compile("(\\d{8})(\\d{8})(\\d{8})(\\d{8})");
		Matcher m = p.matcher(bin);
		if(m.find()){
			dd = Integer.parseInt(m.group(1),2) + "."+
			Integer.parseInt(m.group(2),2) + "." +
			Integer.parseInt(m.group(3),2) + "." +
			Integer.parseInt(m.group(4),2);
		}
		
		return dd;
	}

	public void setBroadcast(long broadcast) {
		this.broadcast = broadcast;
	}

	private long dottedDecimalToDecimal(String dd){
		long n = 0;
		String[] octets = dd.split("\\.");
		n = ((Long.valueOf(octets[0]) * 1) << 24) +
			((Long.valueOf(octets[1]) * 1) << 16) +
			((Long.valueOf(octets[2]) * 1) << 8) +
			(Long.valueOf(octets[3]) * 1);
		return n;
	}
	
	@SuppressWarnings("unused")
	private static boolean isInteger(String s){
		   
		try { 
			Integer.parseInt(s);
		} catch(NumberFormatException e) {
			return false; 
		}
		// only got here if we didn't return false
		return true;
		
	}
	
}
