package mikerp99.network.simulation.data.l3.ip;

import mikerp99.network.simulation.data.PDUInfo;
import mikerp99.network.simulation.data.PDUType;

public class IpInfo extends PDUInfo {

    private IpAddress source;

    public IpInfo(PDUType encapsulatedDataType) {
        super(encapsulatedDataType);
    }

}
