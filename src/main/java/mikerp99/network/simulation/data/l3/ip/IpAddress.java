package mikerp99.network.simulation.data.l3.ip;

import mikerp99.network.simulation.data.PDUAddress;

public class IpAddress extends PDUAddress {

    private String ip;
    private String netmask;
    private IPv4SubnetMath iPv4SubnetMath = new IPv4SubnetMath();

    public IpAddress(String ip, String netmask) {
        this.ip = ip;
        this.netmask = netmask;
        this.iPv4SubnetMath.setNetwork(ip);
        this.iPv4SubnetMath.setSubnetmask(netmask);
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNetmask() {
        return netmask;
    }

    public void setNetmask(String netmask) {
        this.netmask = netmask;
    }
}
