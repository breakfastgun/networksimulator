package mikerp99.network.simulation.data.l3.ip;

import mikerp99.network.simulation.data.PDU;

public class IpPacketPdu<EP extends PDU> extends PDU<IpAddress, EP, IpInfo>{

    public IpPacketPdu(IpAddress address, EP encapsulatedData, IpInfo packetInfo) {
        super(address, encapsulatedData, packetInfo);
    }

}
