package mikerp99.network.simulation.data;

public enum PDUType {

    ETHERNET, IP, TCP, UDP, ICMP

}
