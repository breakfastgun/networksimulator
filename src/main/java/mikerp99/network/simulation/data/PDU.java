package mikerp99.network.simulation.data;

public abstract class PDU<A extends PDUAddress,P extends PDU,I extends PDUInfo> {

    private A address;
    private P encapsulatedData;
    private I packetInfo;

    public PDU(A address, P encapsulatedData, I packetInfo) {
        this.address = address;
        this.encapsulatedData = encapsulatedData;
        this.packetInfo = packetInfo;
    }

    public A getAddress() {
        return address;
    }

    public P getEncapsulatedData() {
        return encapsulatedData;
    }

    public I getPDUInfo() {
        return packetInfo;
    }

    public PDUType getEncapsulatedPacketType(){
        return packetInfo.getEncapsulatedDataType();
    }

    @Override
    public String toString() {
        return String.format("{%s | [%s] | %s}", address, packetInfo, encapsulatedData);
    }
}
