package mikerp99.network.simulation.data;

public class CanBeLabeledPDUInfo extends PDUInfo{

    private Integer label;

    public CanBeLabeledPDUInfo(PDUType encapsulatedDataType){
        super(encapsulatedDataType);
    }

    public CanBeLabeledPDUInfo(PDUType encapsulatedDataType, Integer label) {
        this(encapsulatedDataType);
        this.label = label;
    }

    public int getLabel() {
        return label;
    }

    public void setLabel(int label) {
        this.label = label;
    }
}
