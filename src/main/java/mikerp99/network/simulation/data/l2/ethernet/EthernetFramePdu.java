package mikerp99.network.simulation.data.l2.ethernet;

import mikerp99.network.simulation.data.PDU;

public class EthernetFramePdu<EP extends PDU> extends PDU<MacAddress, EP, EthernetInfo> {

    public EthernetFramePdu(MacAddress address, EP encapsulatedData, EthernetInfo packetInfo) {
        super(address, encapsulatedData, packetInfo);
    }

}
