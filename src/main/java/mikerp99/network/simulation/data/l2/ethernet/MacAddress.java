package mikerp99.network.simulation.data.l2.ethernet;

import mikerp99.network.simulation.data.PDUAddress;

public class MacAddress extends PDUAddress {

    private String mac;

    public MacAddress(String mac) {
        this.mac = mac;
    }

    public String getMac() {
        return mac;
    }

    @Override
    public int hashCode() {
        return mac.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MacAddress){
            return mac.equalsIgnoreCase(((MacAddress) obj).getMac());
        }else {
            return false;
        }
    }

    @Override
    public String toString() {
        return mac;
    }

    public boolean isBroadcast() {
        return mac.equalsIgnoreCase("ffff.ffff.ffff");
    }
}
