package mikerp99.network.simulation.data.l2.ethernet;

import mikerp99.network.simulation.data.CanBeLabeledPDUInfo;
import mikerp99.network.simulation.data.PDUType;

public class EthernetInfo extends CanBeLabeledPDUInfo {

    private MacAddress source;

    public EthernetInfo(MacAddress source, PDUType packetType) {
        super(packetType);
        this.source = source;
    }

    public EthernetInfo(MacAddress source, PDUType packetType, Integer label) {
        super(packetType, label);
        this.source = source;
    }

    public MacAddress getSource() {
        return source;
    }

    @Override
    public String toString() {
        return String.format("src: %s", source);
    }
}
