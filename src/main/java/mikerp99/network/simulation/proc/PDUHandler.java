package mikerp99.network.simulation.proc;

import mikerp99.network.simulation.data.PDU;

public interface PDUHandler<P extends PDU> {

    void receive(P pdu);
    void send(P pdu);

}
