package mikerp99.network.simulation.proc.l2;

import mikerp99.network.simulation.data.l2.ethernet.MacAddress;
import mikerp99.network.simulation.proc.PDUHandler;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SwitchFib {

    private Map<MacAddress, SwitchFibEntry> entries = new HashMap<>();

    ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
    private int ageOut;

    public SwitchFib() {
        this(60);
    }

    public SwitchFib(int ageOut) {
        this.ageOut = ageOut;
        executor.scheduleAtFixedRate(() -> ageOut() , 0, this.ageOut, TimeUnit.SECONDS);
    }

    public void insert(MacAddress macAddress, PDUHandler handler){
        entries.put(macAddress, new SwitchFibEntry(handler));
    }

    public Optional<PDUHandler> getHandler(MacAddress address){
        PDUHandler handler = null;
        SwitchFibEntry switchFibEntry = entries.get(address);
        if (switchFibEntry != null){
            handler = switchFibEntry.getHandler();
        }
        return Optional.ofNullable(handler);
    }

    public void ageOut(){
        List<MacAddress> tooOld = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();
        entries.forEach((m, s) ->{
            if (s.getTimestamp().plusSeconds(ageOut).isAfter(now)){
                tooOld.add(m);
            }
        });
        tooOld.forEach(m->entries.remove(m));
    }

}
