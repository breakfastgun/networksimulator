package mikerp99.network.simulation.proc.l2;

import mikerp99.network.simulation.proc.PDUHandler;

import java.time.LocalDateTime;

class SwitchFibEntry {

    private PDUHandler handler;
    private LocalDateTime timestamp;

    public SwitchFibEntry(PDUHandler handler) {
        this.handler = handler;
        this.timestamp = LocalDateTime.now();
    }

    public PDUHandler getHandler() {
        return handler;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }
}
