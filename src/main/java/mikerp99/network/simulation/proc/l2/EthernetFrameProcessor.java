package mikerp99.network.simulation.proc.l2;

import mikerp99.network.simulation.data.PDU;
import mikerp99.network.simulation.data.PDUInfo;
import mikerp99.network.simulation.data.l2.ethernet.EthernetFramePdu;
import mikerp99.network.simulation.data.l2.ethernet.MacAddress;
import mikerp99.network.simulation.proc.PDUHandler;
import mikerp99.network.simulation.proc.recv.CanRoutePduHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EthernetFrameProcessor extends CanRoutePduHandler<EthernetFramePdu<?>, MacAddress> {

    private SwitchFib fib;
    private List<PDUHandler> handlers = new ArrayList<>();

    public EthernetFrameProcessor(){
        this(60);
    }

    public EthernetFrameProcessor(int ageOut){
        fib = new SwitchFib(ageOut);
    }

    @Override
    public void preReceive(EthernetFramePdu<?> pdu, PDUHandler sender) {
        fib.insert(pdu.getAddress(), sender);
    }

    @Override
    public void doSend(EthernetFramePdu<?> pdu) {
        routeAddress(pdu.getAddress(), pdu.getEncapsulatedData(), pdu.getPDUInfo());
    }

    protected void routeAddress(MacAddress address, PDU data, PDUInfo info){
        if (address.isBroadcast()){
            try {
                Thread.sleep(500); // broadcast storms are bad
                sendToAllHandlers(data);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }else {
            Optional<PDUHandler> optionalPDUHandler = fib.getHandler(address);
            if (optionalPDUHandler.isPresent()) {
                optionalPDUHandler.get().receive(data);
            } else {
                sendToAllHandlers(data);
            }
        }
    }

    public void sendToAllHandlers(PDU data){
        handlers.forEach(handler -> {
            handler.receive(data);
        });
    }

}
