package mikerp99.network.simulation.proc.recv;

import mikerp99.network.simulation.data.PDU;
import mikerp99.network.simulation.proc.PDUHandler;

public class NetworkInterfacePduHandler<P extends PDU> extends AbstractPDUHandler<P>{

    private String id;
    private PDUHandler<P> internal;
    private PDUHandler<P> external;

    public NetworkInterfacePduHandler(String id, PDUHandler<P> internal) {
        this.id = id;
        this.internal = internal;
    }

    public NetworkInterfacePduHandler(String id, PDUHandler<P> internal, PDUHandler<P> external) {
        this.internal = internal;
        this.external = external;
        this.id = id;
    }

    public void receive(P pdu) {
        internal.receive(pdu);
    }

    @Override
    public void doReceive(P pdu) {

    }

    @Override
    public void doSend(P pdu) {
        external.receive(pdu);
    }

    public void setInternal(PDUHandler<P> internal) {
        this.internal = internal;
    }

    public void setExternal(PDUHandler<P> external) {
        this.external = external;
    }
}
