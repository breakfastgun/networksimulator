package mikerp99.network.simulation.proc.recv;

import mikerp99.network.simulation.data.PDU;
import mikerp99.network.simulation.proc.PDUHandler;

import java.util.concurrent.*;

public abstract class AbstractPDUHandler<P extends PDU> implements PDUHandler<P>, Callable<Void> {

    private Boolean debug = false;
    private Boolean enabled = true;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private BlockingQueue<P> queue = new LinkedBlockingQueue<P>();

    public void receive(P pdu){
        if (enabled) {
            queue.add(pdu);
            executorService.submit(this);
        }
    }

    @Override
    public void send(P pdu) {
        if (enabled){
            tapOutput(pdu);
            doSend(pdu);
        }
    }

    public Void call() throws Exception {
        while (!queue.isEmpty()){
            P pdu = queue.poll();
            tapInput(pdu);
            doReceive(pdu);
        }
        return null;
    }

    public void tapInput(P pdu){
    }

    public void tapOutput(P pdu){
    }

    public void preReceive(P pdu, PDUHandler sender){

    }

    public abstract void doReceive(P pdu);

    public abstract void doSend(P pdu);

    public Boolean getDebug() {
        return debug;
    }

    public void setDebug(Boolean debug) {
        this.debug = debug;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
