package mikerp99.network.simulation.proc.recv;

import mikerp99.network.simulation.NetworkInterface;
import mikerp99.network.simulation.data.PDU;
import mikerp99.network.simulation.data.l2.ethernet.EthernetFramePdu;

public class HostNetworkInterfacePDUHandler extends AbstractPDUHandler<EthernetFramePdu<? super PDU>> implements NetworkInterface<EthernetFramePdu<? super PDU>> {

    private String id;
    private String mac;
    private NetworkInterface internal;
    private NetworkInterface external;

    public HostNetworkInterfacePDUHandler(String id, String mac){
        this.id = id;
        this.mac = mac;
    }

    @Override
    public void doReceive(EthernetFramePdu<? super PDU> pdu) {
        if (pdu.getAddress().isBroadcast() || pdu.getAddress().getMac().equalsIgnoreCase(mac)){
            internal.receive(pdu);
        }
    }

    @Override
    public void doSend(EthernetFramePdu<? super PDU> pdu) {
        if (external != null){
            external.receive(pdu);
        }
    }

    public String getId() {
        return id;
    }

    @Override
    public void setOther(NetworkInterface<EthernetFramePdu<? super PDU>> other) {
        this.external = other;
    }
}
