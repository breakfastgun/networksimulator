package mikerp99.network.simulation.proc.recv;

import mikerp99.network.simulation.data.PDU;
import mikerp99.network.simulation.data.PDUAddress;
import mikerp99.network.simulation.data.PDUInfo;

public abstract class CanRoutePduHandler<P extends PDU<A,?,?>, A extends PDUAddress> extends AbstractPDUHandler<P> {

    @Override
    public void doReceive(P pdu) {
        routeAddress(pdu.getAddress(), pdu.getEncapsulatedData(), pdu.getPDUInfo());
    }

    protected abstract void routeAddress(A address, PDU data, PDUInfo info);

}
