package mikerp99.network.simulation.proc.l3;

import mikerp99.network.simulation.data.PDU;
import mikerp99.network.simulation.data.PDUInfo;
import mikerp99.network.simulation.data.l3.ip.IpAddress;
import mikerp99.network.simulation.data.l3.ip.IpPacketPdu;
import mikerp99.network.simulation.proc.recv.CanRoutePduHandler;

public class L3Routing<P extends PDU> extends CanRoutePduHandler<IpPacketPdu<?>, IpAddress> {

    private RoutingFib fib;

    @Override
    protected void routeAddress(IpAddress address, PDU data, PDUInfo info) {

    }

    @Override
    public void doSend(IpPacketPdu<?> pdu) {

    }

}
