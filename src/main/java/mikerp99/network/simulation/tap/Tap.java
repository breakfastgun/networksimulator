package mikerp99.network.simulation.tap;

import lombok.extern.java.Log;
import mikerp99.network.simulation.data.PDU;
import mikerp99.network.simulation.proc.recv.AbstractPDUHandler;

@Log
public class Tap<P extends PDU> extends AbstractPDUHandler<P> {

    private String name;

    public Tap(String name) {
        this.name = name;
    }

    @Override
    public void doReceive(P pdu) {
        log.info(String.format("%s --- %s", name, pdu));
    }

    @Override
    public void doSend(P pdu) {
        // no op
    }

}
