package mikerp99.network.simulation.devices;

import mikerp99.network.simulation.data.l2.ethernet.EthernetFramePdu;
import mikerp99.network.simulation.proc.PDUHandler;
import mikerp99.network.simulation.proc.l2.EthernetFrameProcessor;
import mikerp99.network.simulation.proc.recv.NetworkInterfacePduHandler;

import java.util.ArrayList;
import java.util.List;

public class SimpleSwitch implements Device {

    private List<NetworkInterfacePduHandler<EthernetFramePdu<?>>> interfaceList = new ArrayList<>();
    private EthernetFrameProcessor ethernetFrameProcessor = new EthernetFrameProcessor();

    public SimpleSwitch(int interfaceCount) {
        for (int i = 0; i < interfaceCount; i++){
            String id = String.format("Eth%d", i);
            interfaceList.add(new NetworkInterfacePduHandler<>(id, ethernetFrameProcessor));
        }
    }

    public void connect(PDUHandler<EthernetFramePdu<?>> externalNic, int intId){
        if (intId >= 0 && intId < interfaceList.size()){
            NetworkInterfacePduHandler<EthernetFramePdu<?>> myNic = interfaceList.get(intId);
            myNic.setExternal(externalNic);
        }else {
            // todo
        }
    }

}
