package mikerp99.network.simulation.devices;

import mikerp99.network.simulation.NetworkInterface;
import mikerp99.network.simulation.proc.recv.HostNetworkInterfacePDUHandler;

import java.util.ArrayList;
import java.util.List;

public class Host implements Device{

    private List<HostNetworkInterfacePDUHandler> nics = new ArrayList<>();
    private String macBase;

    public Host(String macBase, int nics) {
        this.macBase = macBase;
        for (int i = 0; i < nics; i++){
            String id = String.format("Eth%d", i);
            String mac = String.format("%s.000%d", this.macBase, i);
            this.nics.add(new HostNetworkInterfacePDUHandler(id, mac));
        }
    }

    @Override
    public void connect(NetworkInterface intf, NetworkInterface other) {
        intf.setOther(other);
        other.setOther(intf);
    }

    @Override
    public NetworkInterface getInterface(String id) {
        NetworkInterface pduHandler = null;
        for (HostNetworkInterfacePDUHandler n : nics) {
            if (n.getId().equalsIgnoreCase(id)){
                pduHandler = n;
            }
        }
        return pduHandler;
    }

    @Override
    public NetworkInterface getInterface(int id) {
        // todo bounds check
        return nics.get(id);
    }

}
