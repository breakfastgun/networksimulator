package mikerp99.network.simulation.devices;

import mikerp99.network.simulation.NetworkInterface;
import mikerp99.network.simulation.proc.PDUHandler;

public interface Device {

    void connect(NetworkInterface intf, NetworkInterface other);
    NetworkInterface getInterface(String id);
    NetworkInterface getInterface(int id);

}
