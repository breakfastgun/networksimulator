package mikerp99.network.simulation;

import mikerp99.network.simulation.data.PDU;
import mikerp99.network.simulation.proc.PDUHandler;

public interface NetworkInterface<P extends PDU> extends PDUHandler<P> {

    void setOther(NetworkInterface<P> other);

}
