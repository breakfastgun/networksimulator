package mikerp99.network;

import mikerp99.network.simulation.data.l3.ip.IPv4SubnetMath;

public class MainApplication {

    public static void main(String[] args){
        IPv4SubnetMath iPv4SubnetMath = new IPv4SubnetMath();
        iPv4SubnetMath.setNetwork("192.168.1.10");
        iPv4SubnetMath.setSubnetmask("255.255.255.128");
        System.out.println(iPv4SubnetMath.getBroadcastDottedDecimal());

    }

}
